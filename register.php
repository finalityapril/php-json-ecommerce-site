<!--Registration Front End-->
<?php
	require_once 'partials/header.php';
?>

<div class="container py-5">
	<div class="row">
		<div class="col-7 mx-auto">
			<div class="card bg-light">
				<div class="card-header"><h3><i class="fas fa-user"></i>Register</h3></div>
				<div class="card-body">
					<form action="assets/lib/new-user.php" method="POST">
						<div class="form-group">
							<label for="txt-firstname">First Name</label>
							<input type="text" class="form-control form-control-lg" name="first_name" required>
						</div>
						<div class="form-group">
							<label for="txt-lastname">Last Name</label>
							<input type="text" class="form-control form-control-lg" name="last_name" required>
						</div>
						<div class="form-group">
							<label for="txt-username">Email</label>
							<input type="email" class="form-control form-control-lg" name="email" required>
						</div>
						<div class="form-group">
							<label for="txt-password">Password</label>
							<input type="password" class="form-control form-control-lg" name="password" required>
						</div>
						<div class="form-group">
							<label for="txt-verify">Verify Password</label>
							<input type="password" class="form-control form-control-lg" name="verify" required>
						</div>
						<button type="submit" class="btn btn-success btn-lg float-right">Login</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
	require_once 'partials/footer.php';
?>
