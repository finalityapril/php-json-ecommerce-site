<?php
	require_once 'partials/header.php';


?>

<div class="container">

	<h2>Add Product</h2>
	<form action="assets/lib/add-item.php" method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<label for="txt-product-name">Product Name: </label>
			<input type="text" class="form-control" name="product_name" required/>
		</div>

		<div class="form-group">
			<label for="txt-price">Price:</label>
			<input type="number" min="0.01" class="form-control" name="price" required/>
		</div>
		<div class="form-group">
			<label for="txt-description">Description:</label>
			<textarea class="form-control" rows="3" name="description"></textarea>
		</div>
		<div class="form-group">
			<label for="txt-file"></label>
			<input type="file" name="product_image" class="form-control" required> 
		</div>
		<button type="submit" class="btn btn-success btn-block">Submit</button>
	</form>
</div>



<?php
	require_once 'partials/footer.php';
?>