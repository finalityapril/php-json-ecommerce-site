<?php
	require_once 'partials/header.php';
?>


<div class="container">
	<div class="row">
		<div class="col-6 mx-auto">
			<h2>Product List</h2>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Item</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
				  <?php
				  	$products_json = file_get_contents('assets/lib/products.json');
				  	$products_arr = json_decode($products_json, true);

				  	for ($id = 0; $id < count($products_arr); $id++) {
				  		echo "<tr>
				  			<td>{$products_arr[$id]['name']}</td>
				  			<td>{$products_arr[$id]['price']}</td>
				  		</tr>";
				  	}
				  ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php
	require_once 'partials/footer.php';
?>