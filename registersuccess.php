<?php
	require_once 'partials/header.php';
?>

<div class="container py-5">
	<div class="row d-block">
		<div class="col-7 mx-auto">
			<div class="alert alert-success" style="font-size: 2rem;"><i class="far fa-check-circle"> </i> <strong>Registration Successful!</strong> </div>
		</div>
	</div>
</div>


<?php
	require_once 'partials/footer.php';
?>