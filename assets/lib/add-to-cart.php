<?php
	ini_set('session.save_path',realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/../session'));

	session_start();

	//ID of the product comes from the add-to-cart.php?item_id=n
	$item_id=$_GET['item_id'];

	//From the form input with the name of quantity
	$quantity=intval($_POST['quantity']);

	/*
		Check first if $_SESSION['cart']['id'] variable is not set yet
		If not yet set, create the variable and assign quantity to the ID of the product.
	*/
	if (!isset($_SESSION['cart'][$item_id])) {
		$_SESSION['cart'][$item_id]=$quantity;
	}
	else {
		$_SESSION['cart'][$item_id]+=$quantity;
	}

	/*
	Create 'message' inside our $_SESSION
	Redirect to index.php and show the newly set session 'message'
	*/

	// $_SESSION ['message'] = '$quantity.'' Items added to cart
	$_SESSION['message'] = "$quantity items added to cart";
	header('location: ../../../index.php');

	//var_dump($_SESSION);


	//echo $quantity.'<br>';

	//echo $item_id;


?>