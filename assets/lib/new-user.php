<?php
	$user_firstname  = htmlspecialchars($_POST['first_name']);
	$user_lastname = htmlspecialchars($_POST['last_name']);
	$user_email  = htmlspecialchars($_POST['email']);
	$user_password  = sha1(htmlspecialchars($_POST['password']));
	$user_checkpassword = sha1(htmlspecialchars($_POST['verify']));
	
	$has_details = false;

	if ($user_firstname != '' && $user_lastname != '' && $user_email != '' && $user_password !='' && $user_checkpassword != '' ) {
		$has_details = true;
	}

	if ($user_password !== $user_checkpassword) {
		echo 'Passwords do not match';
		return;
	
	}

	$new_user = [
		'firstName' => $user_firstname,
		'lastName'=> $user_lastname,
		'email' => $user_email,
		'password' => $user_password,
	];

	$accounts_json = file_get_contents('accounts.json');
	$accounts_arr = json_decode($accounts_json, true);
	
	array_push($accounts_arr, $new_user);
	
	$json_file = fopen('accounts.json', 'w');
	fwrite($json_file, json_encode($accounts_arr, JSON_PRETTY_PRINT));
	fclose($json_file);

	header('location: ../../registersuccess.php');

?>