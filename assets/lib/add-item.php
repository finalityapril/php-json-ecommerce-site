<?php
	//Sanitize form inputs.
	$product_name = htmlspecialchars($_POST['product_name']);
	$price = htmlspecialchars($_POST['price']);
	$description = htmlspecialchars($_POST['description']);

	//Get the file properties
	$file_name = $_FILES['product_image']['name'];
	$file_size = $_FILES['product_image']['size'];
	$file_tmp_name = $_FILES['product_image']['tmp_name'];

	//Get the file extension, convert to lowercase characters
	$file_type = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

	//Variables to check file validity
	$has_details = false;
	$is_image = false;

	//Check if complete details
	if ($product_name !='' && $price > 0) {
		$has_details = true;
	}

	//Check if file is an image
	$is_jpg = ($file_type == 'jpg');
	$is_png = ($file_type == 'png');
	$is_jpeg = ($file_type == 'jpeg');
	$is_gif = ($file_type == 'gif');
	$is_svg = ($file_type == 'svg');

	if ($is_jpg || $is_png || $is_jpeg || $is_gif || $is_gif)   {
		$is_image = true;
	}

	//Check if file is valid
	if ($file_size > 0 && $is_image && $has_details) {
		$final_filepath = 'images/'.$file_name;
		move_uploaded_file($file_tmp_name, $final_filepath);
	}
	else {
		echo 'Please upload an image.';
		return;
	}

	//Create an associative array containing new product information
	$new_product = [
		'name' => $product_name,
		'price' => $price,
		'description' => $description,
		'image' => $final_filepath,
	];

	//Get the contents of products.json (in string format)
	$products_json = file_get_contents('products.json');

	//Convert products.json to an associative array
	$products_arr = json_decode($products_json, true);

	//Add the new product to the existing products array
	array_push($products_arr, $new_product);

	//Open the products.json file for writing content
	$json_file = fopen('products.json', 'w');

	//Write the updated data to the opened file
	fwrite($json_file, json_encode($products_arr, JSON_PRETTY_PRINT));

	//Close the opened file to free up resources
	fclose($json_file);

	header('location: ../../products.php');
?>