<?php
	//Start the session before rmeoving session data
	session_start();

	//Remove all session data
	session_destroy();

	//Redirect to login page
	header('location: ../../login.php');


?>