<?php
	
	//This includes the navbar from the header.
	require_once 'partials/header.php';

	if(isset($_SESSION['email'])) {
		if ($_SESSION['email'] == 'admin@mail.com') {
			header('location: add-product.php');
		}
	}
	else {
		header('location: login.php');
	}

?>

<div class="container">
	<?php
		if(isset($_SESSION['message'])) {
			//Show message here
			//var_dump($_SESSION['message']);
			echo"<div class='alert alert-success alert-dismissable'>
			<a href='#' class='close' data-dismiss='alert'>&times;</a>
			<h4>{$_SESSION['message']}</h4>
			</div>";
			unset($_SESSION['message']);
		}
	?>
	<div class="row">
		<?php
			//Get the products from products.json file
			$products = file_get_contents('assets/lib/products.json');
			
			//Convert it to a PHP array
			$products_array = json_decode($products, true);
			//var_dump($products_array);

			//Iterate and get all the products using for loop
			for ($i = 0; $i <count($products_array); $i++) { ?>
				
				<div class="col-lg-4">	
					<div class="card">
						<img class="card-img-top" src="assets/lib/<?php echo $products_array[$i]['image']; ?>" />
						
						<h4 class="card-title"> <?php echo $products_array[$i]['name']; ?> </h4>

						<p class="card-text">Price:<b>&#8369; <?php echo $products_array[$i]['price'];?> </b></p>
						
						<p class="card-text">Description: <?php echo $products_array[$i]['description'];?></p>
						
						<form action="assets/lib/add-to-cart.php?item_id=<?php echo $i; ?>" method="POST">
							<input type="number" min="1" name="quantity" class="form-control" required>
							<button type="submit" class="btn btn-success btn-lg btn-block">
								<i class="fas fa-plus-circle"></i>Add to Cart
							</button>
						</form>
					</div>
				</div>
	  		<?php } ?>
	</div>
</div>


<?php require_once 'partials/footer.php'; ?>