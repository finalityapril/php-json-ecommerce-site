<?php
  //ini_set('session.save_path',realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/../session'));

  session_start();            
?>

<!DOCTYPE html>
<html lang="en">
  <head>
     <title>PHP Shop</title>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
     <link rel="stylesheet" href="assets/css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="assets/css/style.css">
  </head>
  <body>
     <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
           <a class="navbar-brand" href="index.php"><i class="fas fa-store-alt"></i> What's Shophp?</a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
           </button>
           <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">
                 <!-- Insert Links Here -->
                 <!--
                   <li class="nav-item">
                      <a class="nav-link" href="login.php"><i class="fas fa-lock"></i> Login</a>
                   </li>
                   <li class="nav-item">
                      <a class="nav-link" href="register.php"><i class="far fa-user"></i> Register</a>
                   </li>
                   <li class="nav-item">
                      <a class="nav-link" href="cart.php"><i class="fas fa-shopping-cart"></i> Cart</a>
                   </li>
                   <li class="nav-item">
                      <a class="nav-link" href="assets/lib/processLogout.php"><i class="fas fa-shopping-cart"></i> Logout</a>
                   </li>
                  -->
               <?php if(isset($_SESSION['email'])) { ?>
                   <li class="nav-item">
                      <a class="nav-link" href="cart.php"><i class="fas fa-shopping-cart"></i> Cart</a>
                   </li>
                    <li class="nav-item">
                      <a class="nav-link" href="assets/lib/logout.php"><i class="fas fa-sign-out-alt"></i> Logout</a>
                   </li>

               <?php } else { ?>
                    <li class="nav-item">
                      <a class="nav-link" href="login.php"><i class="fas fa-lock"></i> Login</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="register.php"><i class="far fa-user"></i> Register</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="cart.php"><i class="fas fa-shopping-cart"></i> Cart</a>
                    </li>

               <?php } ?>

              </ul>
           </div>
        </div>
     </nav>
   
