<?php
	require_once 'partials/header.php';

?>

<div class="container py-5">
	<div class="row">
		<div class="col-6 mx-auto">
			<div class="card">
				<div class="card-header"><h3><i class="fas fa-lock"></i>Login</h3></div>
				<div class="card-body">
					<form action="assets/lib/login.php" method="POST">
						<div class="form-group">
							<label for="txt-username">Username</label>
							<input type="email" class="form-control form-control-lg" name="email">
						</div>
						<div class="form-group">
							<label for="txt-password">Password</label>
							<input type="password" class="form-control form-control-lg" name="password">
						</div>
						<button type="submit" class="btn btn-success btn-lg float-right">Login</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
	require_once 'partials/footer.php';

?>
