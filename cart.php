<?php
	require_once 'partials/header.php';
	//ini_set('session.save_path',realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/../session'));
	//session_start();

	if(isset($_SESSION['email'])) {
		if ($_SESSION['email'] == 'admin@mail.com') {
			header('location: add-product.php');
		}
	}
	else {
		header('location:login.php');
	}
	
?>

<div class="container">
	<div class="row">
		<div class="col-6 mx-auto">
			<h4>Cart Items</h4>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Item</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Subtotal</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$products = file_get_contents('assets/lib/products.json');
						$products_arr = json_decode($products,true);
						$total_amount = 0;

						for ($id = 0; $id<count($products_arr); $id++) {
							if (isset($_SESSION['cart'][$id])) {
								$subtotal_amount = ($products_arr[$id]['price'] * $_SESSION['cart'][$id]);
								$total_amount += $subtotal_amount;

								echo "<tr>
									<td>{$products_arr[$id]['name']}</td>
									<td>{$products_arr[$id]['price']}</td>
									<td>{$_SESSION['cart'][$id]}</td>
									<td>{$subtotal_amount}</td>
								</tr>";
							}

						}
						echo "<tr>
							<td colspan='3'>Total Amount:</td>
							<td>&#8369;$total_amount</td>
						</tr>";
					?>
				</tbody>
				
			</table>
		</div>
	</div>
</div>

<?php
	require_once 'partials/footer.php';
?>